<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'v1', 'middleware' => 'oauth'], function () use ($app) {
    $app->group(['prefix' => 'posts'], function () use ($app) {
        $app->get('/', ['uses' => 'PostController@index']);
    });
});
